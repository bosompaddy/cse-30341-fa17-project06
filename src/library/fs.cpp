// fs.cpp: File System

#include "sfs/fs.h"

#include <algorithm>
#include <vector>

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <math.h>
#include <iostream>

std::vector<bool> FileSystem::free_map;
Disk *FileSystem::disk = NULL;
int FileSystem::n_inode_free = 0;


// Auxilliary function used by FileSystem::debug for printing the content of the direct pointers in a given inode
void FileSystem::print_direct_blocks(FileSystem::Inode inode){
    for(int i=0; i<(int)POINTERS_PER_INODE; i++){
        //printf("%d\n", inode.Direct[i]);
        if(inode.Direct[i]){
            printf(" %d", inode.Direct[i]);
        }
        else break;
    }
} 

// Auxilliary function used by FileSystem::debug for printing the content of the indirect pointers in a given inode
void FileSystem::print_indirect_blocks(uint32_t Pointers[]){
    for(int i=0; i<(int)POINTERS_PER_BLOCK; i++){
        if(Pointers[i]){
            printf(" %d", Pointers[i]);
        }
    }
}

// Auxilliary function used by debug to print all inodes in an inode block
void FileSystem::print_inodes(FileSystem::Inode Inodes[], int inode_block, Disk *disk){
    Block block;
    //iterate through all inodes in the block
    for(int i=0; i< (int)INODES_PER_BLOCK; i++){
        //check validity of inode 
        if(Inodes[i].Valid){
            printf("Inode %d:\n", inode_block*INODES_PER_BLOCK + i);
            printf("    size: %u bytes\n"         , Inodes[i].Size);
            printf("    direct blocks:");
            //print the direct blocks
            print_direct_blocks(Inodes[i]);
            printf("\n");
            //check if indirect block exists
            if(Inodes[i].Indirect){
                printf("    indirect block: %u\n", Inodes[i].Indirect);
                disk->read(Inodes[i].Indirect, block.Data);
                printf("    indirect data blocks:");
                //print the indirect pointers
                print_indirect_blocks(block.Pointers);
                printf("\n");
            }
        }
    }
}

// Debug file system -----------------------------------------------------------
/*
This static method scans a mounted filesystem and reports on how the inodes and blocks are organized. 
*/
void FileSystem::debug(Disk *disk) {
    // Read Superblock
    Block block;
    disk->read(0, block.Data);

    //check magic number
    printf("SuperBlock:\n");
    if (block.Super.MagicNumber == FileSystem::MAGIC_NUMBER){
        printf("    magic number is valid\n");
    }
    else{
        printf("    magic number is invalid\n");
        exit(0);
    }

    // Print content of the superblock
    printf("    %u blocks\n"         , block.Super.Blocks);
    printf("    %u inode blocks\n"   , block.Super.InodeBlocks);
    printf("    %u inodes\n"         , block.Super.Inodes);
    
    // Read Inode blocks
    Block inode_block;
    for(int i=1; i<= (int)block.Super.InodeBlocks; i++){
        disk->read(i, inode_block.Data);
        //print inodes in each block
        print_inodes(inode_block.Inodes, i-1, disk);
    }
}

// Format file system ----------------------------------------------------------
/*
This static method Creates a new filesystem on the disk, destroying any data already present. 
It should set aside ten percent of the blocks for inodes, clear the inode table, and write the superblock. It must return true on success, false otherwise.
Note: formatting a filesystem does not cause it to be mounted. Also, an attempt to format an already-mounted disk should do nothing and return failure.
*/
bool FileSystem::format(Disk *disk) {
    // IF disk is already mounted then return failure
    if(disk->mounted()){
        return false;
    }

    // Write superblock
    Block super_block;
    memset(super_block.Data, 0, sizeof(super_block.Data));
    super_block.Super.MagicNumber = MAGIC_NUMBER;
    super_block.Super.Blocks = disk->size();
    super_block.Super.InodeBlocks = ceil(super_block.Super.Blocks/10.0);
    super_block.Super.Inodes = (INODES_PER_BLOCK)*super_block.Super.InodeBlocks;
    disk->write(0, super_block.Data);
    
    // Clear all other blocks
    Block block;
    memset(block.Data, 0, sizeof(block.Data));
    for(int i=1; i<(int)super_block.Super.Blocks; i++){
        disk->write(i, block.Data);
    }

    // Initialize free_map
    FileSystem::free_map = std::vector<bool>(disk->size(), 0);
    FileSystem::free_map[0] = 1;

    // Initialize n_inode_free
    FileSystem::n_inode_free = super_block.Super.Inodes; 
    
    return true;
}

// Auxilliary function used by FileSystem::init_freemap that sets free_map = 1 for all the direct blocks in a given inode
void FileSystem::freemap_direct_blocks(FileSystem::Inode inode){
    for(int i=0; i<(int)POINTERS_PER_INODE; i++){
        if(inode.Direct[i]){
            free_map[inode.Direct[i]]=1;
        }
        else break;
    }
} 

// Auxilliary function used by FileSystem::init_freemap that sets free_map = 1 for all the indirect blocks in a given inode
void FileSystem::freemap_indirect_blocks(uint32_t Pointers[]){
    for(int i=0; i<(int)POINTERS_PER_BLOCK; i++){
        if(Pointers[i]){
            free_map[Pointers[i]]=1;
        }
    }
}

// Auxilliary function used by FileSystem::mount to set free_map  = 1 for all the direct blocks and indirect blocks in a given inode block. Additionally updates n_inode_free accordingly
void FileSystem::init_freemap(FileSystem::Inode Inodes[], Disk *disk){
    Block indirect_block;
    for (int i=0; i<(int)INODES_PER_BLOCK; i++){
        if(Inodes[i].Valid){
            freemap_direct_blocks(Inodes[i]);
            if(Inodes[i].Indirect){                    
                disk->read(Inodes[i].Indirect, indirect_block.Data);
                freemap_indirect_blocks(indirect_block.Pointers);
                free_map[Inodes[i].Indirect]=1;
            }
            n_inode_free--;
        }
    }
}

// Mount file system -----------------------------------------------------------
/*
This method examines the disk for a filesystem. If one is present, read the superblock, build a free block bitmap, and prepare the filesystem for use. 
Return true on success, false otherwise.
Note: a successful mount is a pre-requisite for the remaining calls.
*/
bool FileSystem::mount(Disk *disk) {
    //check if its already mounted
    if(disk->mounted()){
        return false;
    }
    
    // Read superblock
    Block super_block;
    disk->read(0, super_block.Data);
    
    //check magic number
    if(super_block.Super.MagicNumber!=MAGIC_NUMBER){
        return false;
    }

    // check whether number of blocks > 0
    if(super_block.Super.Blocks == 0){
        return false;
    }

    // Check consistency of superblock entries
    if(super_block.Super.InodeBlocks*INODES_PER_BLOCK != super_block.Super.Inodes or super_block.Super.Blocks < super_block.Super.InodeBlocks){
        return false;
    }

    // Initialize free_map to all 1's i.e. all blocks are initially free
    FileSystem::free_map = std::vector<bool>(disk->size(), 0);
    FileSystem::free_map[0] = 1;
    int n_inode_blocks = (int)super_block.Super.InodeBlocks;
    // Set device and mount
    disk->mount();
    // Copy metadata

    // Allocate free block bitmap
    Block inode_block;
    //iterate through inode blocks
    n_inode_free = super_block.Super.Inodes;
    for(int i=1; i<=n_inode_blocks; i++){
        disk->read(i, inode_block.Data);
        free_map[i]=1;
        init_freemap(inode_block.Inodes, disk);
    }
    FileSystem::disk = disk;
    return true;
}

// Create inode ----------------------------------------------------------------
/*
This method Creates a new inode of zero length. On success, return the inumber. On failure, return -1.
*/
ssize_t FileSystem::create() {
    // Locate free inode in inode table
    Block super_block;
    // Read superblock
    disk->read(0, super_block.Data);
    int n_inode_blocks = (int)super_block.Super.InodeBlocks;
    Block inode_block;
    if(n_inode_free == 0){
        return -1;
    }
    //iterate through inode blocks
    for(int i=1; i<=n_inode_blocks; i++){
        disk->read(i, inode_block.Data);
        for(int j=0; j<(int)INODES_PER_BLOCK; j++){
            if(inode_block.Inodes[j].Valid == 0){
                inode_block.Inodes[j].Valid = 1;
                disk->write(i, inode_block.Data);
                n_inode_free--;
                return (i-1)*(int)INODES_PER_BLOCK+j;
            }
        }
    }

    // Record inode if found
    return -1;
}

// Remove inode ----------------------------------------------------------------
/*
This method removes the inode indicated by the inumber. 
It should release all data and indirect blocks assigned to this inode and return them to the free block map. On success, it returns true. On failure, it returns false.
*/
bool FileSystem::remove(size_t inumber) {
    // Load inode information
    Block inode_block;
    int block_number = (inumber)/INODES_PER_BLOCK + 1;
    int offset = inumber%INODES_PER_BLOCK;
    //read the inode block
    disk->read(block_number, inode_block.Data);
    Inode curr = inode_block.Inodes[offset];
    if(curr.Valid == 0){
        return false;
    }
    Block super_block;
    // Read superblock
    disk->read(0, super_block.Data);
    // Free direct blocks
    for(int i=0; i<(int)POINTERS_PER_INODE; i++){
        if(curr.Direct[i])
        free_map[curr.Direct[i]]=0;
        inode_block.Inodes[offset].Direct[i] = 0;
    }
    // Free indirect blocks
    Block indirect;
    if(curr.Indirect){
        disk->read(curr.Indirect, indirect.Data);
        for(int i=0; i<(int)POINTERS_PER_BLOCK; i++){
            if(indirect.Pointers[i])
            free_map[indirect.Pointers[i]]=0;
        }
        memset(indirect.Data, 0, sizeof(indirect.Data));
        disk->write(curr.Indirect, indirect.Data);
        free_map[curr.Indirect]=0;
    }
    // Clear inode in inode table
    inode_block.Inodes[offset].Valid = 0;
    inode_block.Inodes[offset].Size = 0;
    inode_block.Inodes[offset].Indirect = 0;
    disk->write(block_number, inode_block.Data);
    n_inode_free++;
    return true;
}

// Inode stat ------------------------------------------------------------------
/*
This method returns the logical size of the given inumber, in bytes. Note that zero is a valid logical size for an inode. On failure, it returns -1.
*/
ssize_t FileSystem::stat(size_t inumber) {
    // Load inode information
    // Read superblock
    Block inode_block;
    int block_number = (inumber)/INODES_PER_BLOCK + 1;
    int offset = inumber%INODES_PER_BLOCK;
    //read the inode block
    if(block_number > (int)disk->size()){
        return -1;
    }
    disk->read(block_number, inode_block.Data);
    if(inode_block.Inodes[offset].Valid){
        return inode_block.Inodes[offset].Size;
    }
    return -1;
}

// Read from inode -------------------------------------------------------------
/*
This method reads data from a valid inode. It then copies length bytes from the data blocks of the inode into the data pointer, starting at offset in the inode. 
It should return the total number of bytes read. If the given inumber is invalid, or any other error is encountered, the method returns -1.
Note: the number of bytes actually read could be smaller than the number of bytes requested, perhaps if the end of the inode is reached.
*/
ssize_t FileSystem::read(size_t inumber, char *data, size_t length, size_t offset) {
    // Load inode information
    Block inode_block;
    int block_number = (inumber)/INODES_PER_BLOCK + 1;
    int inode_offset = inumber%INODES_PER_BLOCK;

    if(block_number > (int)disk->size()){
        return -1;
    }
    disk->read(block_number, inode_block.Data);
    Inode inode = inode_block.Inodes[inode_offset];
    if(inode.Valid == 0){
        disk->read(0, inode_block.Data);
        return -1;
    }
    // Adjust length
    int file_len = inode.Size;
    if((int)offset >= file_len){
        return -1;
    }
    if(length == 0){
        return 0;
    }
    if ((int)(length + offset) > file_len){
        length = file_len - offset;
    }    
    // Read block and copy to data
    std::string d = "";
    int sz = (int)Disk::BLOCK_SIZE;
    int rem = offset%sz;
    int round_down = offset - rem;
    int start_block_num = round_down/sz;
    int end_block_num = floor((offset+length-1)/(sz*1.0));
    int num_direct = std::min((int)POINTERS_PER_INODE-1, end_block_num)+1;
    char curr[sz];
    for(int i=start_block_num; i<num_direct; i++){
        disk->read(inode.Direct[i], curr);
        d+=curr;
    }
    Block indirect;
    if(inode.Indirect){
        disk->read(inode.Indirect, indirect.Data);
        for(int j=std::max(start_block_num-5,0); j<end_block_num-num_direct+1; j++){
            disk->read(indirect.Pointers[j], curr);
            d+=curr;
        }
    }
    std::string buff = d.substr(rem, length);
    buff.erase(std::find(buff.begin(), buff.end(), '\0'), buff.end());
    strcpy(data, (buff.c_str()));
    return length;
}

// Auxilliary function. Populate destination with source[beg...beg + n]
void FileSystem::substring(char *destination, const char *source, int beg, int n){
    while(n > 0){
        *destination = *(source + beg);
        destination++;
        source++;
        n--;
    }
}

// Auxilliary function used by FileSystem::write. Writes data into the corresponding block (direct/indirect) as indexed by block_index (linear indexing into the 1029 data blocks). 
// Additionally increments bytes_copied accordingly.
void FileSystem::write_to_block(int block_index, FileSystem::Inode inode, FileSystem::Block indirect, char* data, int* bytes_copied){
    if(block_index < (int)POINTERS_PER_INODE) {
        disk->write(inode.Direct[block_index], data);
    }
    else {
        disk->write(indirect.Pointers[block_index - POINTERS_PER_INODE], data);
    }
    *bytes_copied += Disk::BLOCK_SIZE;
}

// Auxilliary function used by FileSystem::write. Reads from the corresponding block (direct/indirect) as indexed by block_index (linear indexing into the 1029 data blocks) into data. 
void FileSystem::read_from_block(int block_index, FileSystem::Inode inode, FileSystem::Block indirect, char* data){
    if(block_index < (int)POINTERS_PER_INODE) {
        disk->read(inode.Direct[block_index], data);
    }
    else {
        disk->read(indirect.Pointers[block_index - POINTERS_PER_INODE], data);
    }
}

// Auxilliary function used by FileSystem::write. Populates block_indices with num_blocks free blocks if they are available, else populates block_indices with as many new blocks as can be 
// allocated. Additionally updates the free_map bitmap accordingly. 
void FileSystem::get_free_blocks(int num_blocks, std::vector<int> &block_indices){
    int i = 0;
    while(num_blocks > 0){
        if(i >= (int)free_map.size())
            break;
        if(free_map[i] == 0){
            free_map[i] = 1;
            block_indices.push_back(i);
            num_blocks--;
        }
        i++;
    }
}

// Write to inode --------------------------------------------------------------
/*
This method writes data to a valid inode by copying length bytes from the pointer data into the data blocks of the inode starting at offset bytes. 
It will allocate any necessary direct and indirect blocks in the process. Afterwards, it returns the number of bytes actually written. 
If the given inumber is invalid, or any other error is encountered, return -1.
Note: the number of bytes actually written could be smaller than the number of bytes request, perhaps if the disk becomes full.
*/
ssize_t FileSystem::write(size_t inumber, char *data, size_t length, size_t offset) {
    // inode block calculations
    Block inode_block;
    int block_number = (inumber)/INODES_PER_BLOCK + 1;
    int inode_offset = inumber%INODES_PER_BLOCK;
    // sanity check for inumber
    if(block_number > (int)disk->size()){
        return -1;
    }

    // read inode block and load inode information
    disk->read(block_number, inode_block.Data);
    Inode inode = inode_block.Inodes[inode_offset];

    // validity check for inode
    if(inode.Valid == 0){
        disk->read(0, inode_block.Data);
        return -1;
    }

    // current file length
    int file_len = inode.Size;

    // cannot write to arbitrary positions
    if((int)offset > file_len){
        return -1;
    }

    // no write required in this case
    if(length == 0){
        return 0;
    }

    // might potentially need to allocate data blocks for writing
    std::vector<int> direct_blocks;
    std::vector<int> indirect_pointer_block;
    std::vector<int> indirect_data_blocks;
    
    int sz = (int)Disk::BLOCK_SIZE;
    int start_block_index = floor((int)offset/sz); // we need to start writing from this block onwards
    int file_len_block_index = floor((file_len - 1)/(float)sz); // file currently ends at this block index
    int end_block_index = std::min((int)floor(((int)offset + length - 1)/sz), (int)(POINTERS_PER_INODE + POINTERS_PER_BLOCK)); // we need to write upto and including this block

    // load the current set of indirect Pointers if necessary
    Block indirect;
    memset(indirect.Data, 0, sizeof(indirect.Data));
    if(file_len_block_index >= (int)POINTERS_PER_INODE){
        disk->read(inode.Indirect, indirect.Data);
    }

    bool indirect_changed = false;
    if(end_block_index > file_len_block_index){ // we need to allocate new blocks from file_len_block_index + 1 -> end_block_index (inclusive)
        if(file_len_block_index < (int)POINTERS_PER_INODE){ // need to allocate some direct blocks
            int num_direct_alloc = std::min((int)end_block_index, (int)POINTERS_PER_INODE - 1) - file_len_block_index;
            get_free_blocks(num_direct_alloc, direct_blocks);
            int i = file_len_block_index + 1;
            for(auto pointer : direct_blocks){
                file_len += sz;
                inode.Direct[i] = (uint32_t)pointer;
                i++;
            }
            if(end_block_index >= (int)POINTERS_PER_INODE){ // we need to allocate a block for the indirect pointers as well as some blocks for the data
                get_free_blocks(1, indirect_pointer_block);
                if(indirect_pointer_block.size() != 0){
                    inode.Indirect = (uint32_t)indirect_pointer_block[0];
                    get_free_blocks(end_block_index - POINTERS_PER_INODE + 1, indirect_data_blocks);
                    int i = 0;
                    for(auto pointer : indirect_data_blocks){
                        indirect_changed = true;
                        file_len += sz;
                        indirect.Pointers[i] = (uint32_t)pointer;
                        i++;
                    }
                }
            }    
        } 
        else{
            get_free_blocks(end_block_index - file_len_block_index, indirect_data_blocks);
            int i = file_len_block_index - (int)POINTERS_PER_INODE + 1;
            for(auto pointer : indirect_data_blocks){
                indirect_changed = true;
                file_len += sz;
                indirect.Pointers[i] = (uint32_t)pointer;
                i++;
            }
        }
        if(file_len > (int)(offset + length))
            file_len = (int)(offset + length);
        else{
            end_block_index = floor((file_len - 1)/sz);   
        }
        file_len_block_index = floor((file_len - 1)/sz); // file length block index after allocation of new space. 
    }

    int bytes_copied = 0;
    int actual_bytes_copied = 0;
    char* block_data = new char[sz];

    // write into the first block
    int offset_mod = ((int)offset)%sz; // offset in start_block_index from which data must be written
    read_from_block(start_block_index, inode, indirect, block_data); // fetch data and store in block_data
    int upper_limit = 0;
    if(start_block_index == end_block_index){
        upper_limit = offset_mod + (int)length;
    }
    else{
        upper_limit = sz;
    }
    for(int i = offset_mod; i < upper_limit; i++){
        block_data[i] = data[i - offset_mod];
    }
    
    actual_bytes_copied += (upper_limit - offset_mod);
    write_to_block(start_block_index, inode, indirect, block_data, &bytes_copied);
    // writing to the rest of the blocks
    for(int i = start_block_index + 1; i <= end_block_index; i++){
        if(i == end_block_index){
            read_from_block(end_block_index, inode, indirect, block_data);
            int upper_lim = std::min(sz, (int)length - bytes_copied);
            substring(block_data, data, bytes_copied, upper_lim);
            actual_bytes_copied += upper_lim;
            write_to_block(i, inode, indirect, block_data, &bytes_copied);
        }
        else{
            substring(block_data, data, bytes_copied, sz);
            write_to_block(i, inode, indirect, block_data, &bytes_copied);
            actual_bytes_copied += sz;
        }
    }

    // write back to disk
    if(indirect_changed == true){
        disk->write(inode.Indirect, indirect.Data);
    }
    inode.Size = file_len;
    inode_block.Inodes[inode_offset] = inode;
    disk->write(block_number, inode_block.Data);
    delete [] block_data;
    return actual_bytes_copied;
}
