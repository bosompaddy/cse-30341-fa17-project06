// fs.h: File System

#pragma once

#include "sfs/disk.h"
#include <vector>

#include <stdint.h>

class FileSystem {
public:
    const static uint32_t MAGIC_NUMBER	     = 0xf0f03410;
    const static uint32_t INODES_PER_BLOCK   = 128;
    const static uint32_t POINTERS_PER_INODE = 5;
    const static uint32_t POINTERS_PER_BLOCK = 1024;

private:
    struct SuperBlock {		// Superblock structure
    	uint32_t MagicNumber;	// File system magic number
    	uint32_t Blocks;	// Number of blocks in file system
    	uint32_t InodeBlocks;	// Number of blocks reserved for inodes
    	uint32_t Inodes;	// Number of inodes in file system
    };

    struct Inode {
    	uint32_t Valid;		// Whether or not inode is valid
    	uint32_t Size;		// Size of file
    	uint32_t Direct[POINTERS_PER_INODE]; // Direct pointers
    	uint32_t Indirect;	// Indirect pointer
    };

    union Block {
    	SuperBlock  Super;			    // Superblock
    	Inode	    Inodes[INODES_PER_BLOCK];	    // Inode block
    	uint32_t    Pointers[POINTERS_PER_BLOCK];   // Pointer block
    	char	    Data[Disk::BLOCK_SIZE];	    // Data block
    };

    // TODO: Internal helper functions
    static void print_inodes(Inode Inodes[], int inode_block, Disk *disk);
    static void print_direct_blocks(Inode inode);
    static void print_indirect_blocks(uint32_t Pointers[]);
    static void init_freemap(Inode Inodes[], Disk *disk);
    static void freemap_direct_blocks(Inode inode);
    static void freemap_indirect_blocks(uint32_t Pointers[]);
    static void write_to_block(int block_index, Inode inode, Block indirect, char* data, int* bytes_copied);
    static void read_from_block(int block_index, Inode inode, Block indirect, char* data);
    static void substring(char *destination, const char *source, int beg, int n);
    static void get_free_blocks(int num_blocks, std::vector<int> &block_indices);
    static void write_to_inode(uint32_t pointer,  Block& indirect, int block_index, Inode &inode);

    // TODO: Internal member variables
    static std::vector<bool> free_map;
    static Disk *disk;
    static int n_inode_free;

public:
    static void debug(Disk *disk);
    static bool format(Disk *disk);

    bool mount(Disk *disk);

    ssize_t create();
    bool    remove(size_t inumber);
    ssize_t stat(size_t inumber);

    ssize_t read(size_t inumber, char *data, size_t length, size_t offset);
    ssize_t write(size_t inumber, char *data, size_t length, size_t offset);
    void clear_freemap(){
        free_map.clear();
    }
};
