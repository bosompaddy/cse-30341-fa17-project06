CS3500 July-Nov 2020: Course Project
==========================

This is the documentation for Simple File System - Course Project of CS3500 Operating Systems July-Nov 2020.

Members
-------

1. Aniswar Srivatsa Krishnan (cs18b050@smail.iitm.ac.in)
2. Shreesha G Bhat (cs18b103@smail.iitm.ac.in)

Design
------

> 1. To implement `Filesystem::debug`, you will need to load the file system
>    data structures and report the **superblock** and **inodes**.
>
>       - How will you read the superblock?
        We know that superblock is present at block number 0 in the disk. Hence we can use Disk.read(0, data) to read superblock into data
>       - How will you traverse all the inodes?
        We can get the number of inode blocks from Superblock.InodeBlocks, and we know that each inode block containes INODES_PER_BLOCK. The inode blocks are present from block numbers 1 to Superblock.InodeBlocks. In each inode block we can traverse the Block.Inodes array
>       - How will you determine all the information related to an inode?
        We can check the validity of the Inode using Inode.Valid and size of the file pointed to by using Inode.size. The direct blocks are available in Inode.Direct and the indirect pointer is in Inode.Indirect.
>       - How will you determine all the blocks related to an inode?
        We have POINTERS_PER_INODE direct pointers in the array Inode.Direct and an indirect pointer in Inode.Indirect, which points to a data block containing POINTERS_PER_BLOCK pointers. Each pointer points to a data block which contains a part of the file.

> 2. To implement `FileSystem::format`, you will need to write the superblock
>    and clear the remaining blocks in the file system.
>
>       - What pre-condition must be true before this operation can succeed?
        The file system should not have been mounted before formatting. We can check this condition using Disk.mounted()
>       - What information must be written into the superblock?
        The magic number, number of blocks, number of inode blocks, and the number of inodes must be written in the superblock
>       - How would you clear all the remaining blocks?
        We can write 0 to all the blocks except the super block to clear them

> 3. To implement `FileSystem::mount`, you will need to prepare a filesystem
>    for use by reading the superblock and allocating the free block bitmap.
>
>       - What pre-condition must be true before this operation can succeed?
        The disk should not be already mounted. 
>       - What sanity checks must you perform?         
        The magic number in the disk must match the magic number of our file system. The number of inodes and the number of inode blocks in the superblock must be consistent as we have a fixed number of inodes per block.
>       - How will you record that you mounted a disk?
        We can call the function Disk.mounted()
>       - How will you determine which blocks are free?
        We go through all the valid inodes, the direct blocks of them and the indirect blocks of them and set these blocks as occupied. All the other blocks are considered to be free.

> 4. To implement `FileSystem::create`, you will need to locate a free inode
>    and save a new inode into the inode table.
>
>       - How will you locate a free inode?
        We can identify the inodes with Valid bit 0, which means they are not associated with any file.
>       - What information would you see in a new inode?
        A newly created inode will have the valid bit set to 1 and all other fields set to zero, since no data or data blocks are allocated to it.
>       - How will you record this new inode?
        We have a variable which records the number of free inodes; we can decrement the variable.

> 5. To implement `FileSystem::remove`, you will need to locate the inode and
>    then free its associated blocks.
>
>       - How will you determine if the specified inode is valid?
        We can determine the validity of the inode by checking the valid bit in the inode.
>       - How will you free the direct blocks?
        We will clear the bit in the bitmap corresponding to each direct block present in the file and we will have to set all the direct blocks to 0.
>       - How will you free the indirect blocks?
        If the indirect pointer is non zero then we will have to read in the indirect pointers block and for each indirect data block, we will have to clear the bit in the bitmap corresponding to it. Then, We will have to clear the indirect pointers block and set the indirect pointer in the inode to zero. 
>       - How will you update the inode table?
        We have a variable which records the number of free inodes; we can decrement the variable.


> 6. To implement `FileSystem::stat`, you will need to locate the inode and
>    return its size.
>
>       - How will you determine if the specified inode is valid?
        We can determine the validity of the inode by checking the valid bit in the inode.
>       - How will you determine the inode's size?
        We can use the size field in the inode to determine the inode's size.

> 7. To implement `FileSystem::read`, you will need to locate the inode and
>    copy data from appropriate blocks to the user-specified data buffer.
>
>       - How will you determine if the specified inode is valid?
        We can determine the validity of the inode by checking the valid bit in the inode.
>       - How will you determine which block to read from?
        We first find the block in which the inode corresponding to the inode is present. We read that block and get the inode by indexing into that block. We then get the information about the direct and indirect blocks of the file from the inode, from which we can read the data.
>       - How will you handle the offset?
        The data in the file is arranged first in the 5 direct blocks, and then the indirect blocks. The indirect blocks' location is present in a block containing pointers to all the indirect blocks. We use the offset to find the block from which to start reading and the location inside that file from which to start copying to buffer.
>       - How will you copy from a block to the data buffer?
        Once we have found the first block to read from, we read blocks until we reach the required length or the end of the file and append to a string, we truncate the string to the required , copy it into the buffer provided as the argument and return the number of bytes read.

> 8. To implement `FileSystem::write`, you will need to locate the inode and
>    copy data the user-specified data buffer to data blocks in the file
>    system.
>
>       - How will you determine if the specified inode is valid?
        We can determine the validity of the inode by checking the valid bit in the inode.
>       - How will you determine which block to write to?
        We first find the block in which the inode corresponding to the inode is present. We read that block and get the inode by indexing into that block. We then get the information about the direct and indirect blocks of the file from the inode, tpo which we can write the data.
>       - How will you handle the offset?
        The data in the file is arranged first in the 5 direct blocks, and then the indirect blocks. The indirect blocks' location is present in a block containing pointers to all the indirect blocks. We use the offset to find the block from which to start writing and the location inside that file from which to start writing the given data.
>       - How will you know if you need a new block?
        If offset + length is greater than the current file size rounded up to the nearest block, this means that we will need a new block, as we will exceed the current allocated blocks when writing data to the file.
>       - How will you manage allocating a new block if you need another one?
        We have maintained a bitmap indicating whether a block is free or not. We do a search in this bitmap to find a free block and allocate it to the file, if the number of the blocks allocated to the file exceeds POINTERS_PER_INODE, then we will also have to allocate an indirect pointers block, which will contain pointers to all the indirect data blocks of the file.
>       - How will you copy from the data buffer to a block?
        Once we have found the first block to write to, we write to blocks until we reach the required length or the maximum file length, allocating blocks as and when required. We then return the number of bytes written
>       - How will you update the inode?
        We have to update the size field in the inode to the new size of the block after writing data to it.


Reference
-------

https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project06.html

